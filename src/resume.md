# Jason Azze

#### Summary
This is my resume. I like to eat hamburgers.

## Experience

### Pizza Baker
- Made pizzas
- Sold them to unsuspecting passers by
- Profited

### Bagel Baker
- Made Bagels
- Ate most of them before any customers arrived

### Assistant Croissant Taster
- Taste tested pastry
- It was delicious
